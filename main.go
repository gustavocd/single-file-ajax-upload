package main

import (
	"log"
	"net/http"

	"bitbucket.org/gustavocd/upload-img/controllers"
)

func main() {
	http.Handle("/", http.FileServer(http.Dir("./public")))
	http.HandleFunc("/upload", controllers.UploadFile)
	log.Println("Running")
	http.ListenAndServe(":8080", nil)
}
