# Single file ajax upload #

This is a simple project where you can see an example about how to process Ajax request with Golang and leverage axios library as well as a little of CSS animations.

### What is this repository for? ###

* Practice Ajax requests.
* Process files in the server with Golang.

### How do I get set up? ###

* Clone the repository
* Enter to the folder
* Run `go run main.go`
* Go to `http://localhost:8080`
* Click on the input field
* Pick an image and you should see a success/error message

### Contribution guidelines ###

* Feel free to change this code and improve it, just let me know if you get it better :smiley: